##
## Build ##
##

FROM golang:1.21-alpine AS build

ENV SERVICE_NAME echo-news
ENV REPOSITORY gitlab.com/zakariawahyu

WORKDIR "/go/src/${REPOSITORY}/${SERVICE_NAME}"

COPY . .
RUN go build -o /usr/bin/${SERVICE_NAME}

##
## Deploy
##

FROM alpine

ENV SERVICE_NAME echo-news
ENV REPOSITORY gitlab.com/zakariawahyu

RUN apk add --no-cache tzdata

COPY --from=build /usr/bin/${SERVICE_NAME} /usr/bin/${SERVICE_NAME}

RUN adduser -D -g '' echonews

USER echonews

ENTRYPOINT ["/usr/bin/echo-news"]
